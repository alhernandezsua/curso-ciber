import matplotlib.pyplot as plt

def matriz_imprimir(confmat):
	fig, ax = plt.subplots(figsize=(2.5, 2.5))
	ax.matshow(confmat, cmap=plt.cm.Blues, alpha=0.3)
	for i in range(confmat.shape[0]):
	    for j in range(confmat.shape[1]):
	        ax.text(x=j, y=i,
	                s=confmat[i, j],
	                va='center', ha='center')
	plt.xlabel('etiqueta predicha')
	plt.ylabel('etiqueta verdadera')
	plt.show()